import Domain.*;
import Service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class UserServiceTest {

   @Mock
   private User user;

   @Mock
   private Person person;

   @Mock
   private Address address;

   @Mock
   private Role role;

   @Mock
   private Permission permission;

   @Before
   public void initMocks() {
      MockitoAnnotations.initMocks(this);
   }

   @Test
   public void it_correctly_returns_users_with_2_or_more_addresses(){
      when(user.getPersonDetails()).thenReturn(person);
      when(user.getName()).thenReturn("user1");

      when(person.getAddresses()).thenReturn(Arrays.asList(address, address)).thenReturn(Collections.singletonList(address));

      assertThat(UserService.findUsersWhoHaveMoreThanOneAddress(Arrays.asList(user, user))).isEqualTo(Collections.singletonList(user));

   }
   /*
   @Test
   public void if_address_is_null(){
     when(user.getPersonDetails()).thenReturn(person);
     when(user.getPersonDetails().getAddresses()).thenReturn(null);
     when(user.getName()).thenReturn("user1");

     assertThat(UserService.findUsersWhoHaveMoreThanOneAddress(Arrays.asList(user,user))).isNotNull();
   }*/

   @Test
   public void is_correctly_returns_users_above_18(){
      when(user.getPersonDetails()).thenReturn(person);
      when(person.getAge()).thenReturn(20,23,15);
      when(person.getName()).thenReturn("kappa");
      when(person.getSurname()).thenReturn("kappacino");

      String above18 = UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(Arrays.asList(user,user,user));

      assertThat(above18).isEqualTo("kappa kappacino and kappa kappacino are of legal age here.");

   }

   @Test
   public void it_correctly_returns_oldest_person() {
      when(user.getPersonDetails()).thenReturn(person);
      when(person.getAge()).thenReturn(1, 2, 3, 4, 5);
      Person oldestPerson = UserService.findOldestPerson(Arrays.asList(user, user, user, user, user));

      assertThat(oldestPerson.getAge()).isEqualTo(5);
   }

   @Test
   public void it_correctly_returns_user_with_longest_username() {
      when(user.getName()).thenReturn("short", "longer", "longest");

      User userWithLongestUsername = UserService.findUserWithLongestUsername(Arrays.asList(user, user, user));

      assertThat(userWithLongestUsername.getName()).isEqualTo("longest");
   }
/*
   @Test
   public void it_correctly_returns_Sorted_Permissions_Of_Users_With_Name_Starting_With_A(){
      when(user.getPersonDetails()).thenReturn(person);
      when(user.getName()).thenReturn("Aamk","Adaksm","Nope");
      when(permission.getName()).thenReturn("Kappa","appacino","bappucino");
      when(user.getPersonDetails().getRole().getPermissions()).thenReturn(Arrays.asList(permission,permission,permission));

      List<String> sortedPermissions =  UserService.getSortedPermissionsOfUsersWithNameStartingWithA(Arrays.asList(user,user,user));

      assertThat(sortedPermissions).isEqualTo(Arrays.asList("appacino,appacino,bappacino,bappacino,Kappa,Kappa"));
      // Nie wiem jak to skończyć, wywala nullpointera :/
   }

   */

   @Test
   public void it_correctly_returnsPermissionNamesOfUsersWithSurnameStartingWithS(){



   }

   @Test
   public void it_correctly_returns_groupUsersByRole(){

   }

   @Test
   public void it_correctly_returns_partitionUserByUnderAndOver18(){

   }
}
