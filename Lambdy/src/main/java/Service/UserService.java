package Service;

import Domain.*;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class UserService {

    public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {

       users = users.stream()
                .filter(user->user.getPersonDetails().getAddresses().size()>1)
                .collect(Collectors.toList());

        for(User user: users){
            System.out.println(user.getName());
        }

        return users;

    }

    public static Person findOldestPerson(List<User> users) {


        /*Person oldestUser = users.stream()
                .map(u->u.getPersonDetails())
                .max(Comparator.comparingInt(Person::getAge))
                .get();*/

          User oldestUser = users.stream()
                .reduce((p1, p2) -> p1.getPersonDetails().getAge() > p2.getPersonDetails().getAge() ? p1 : p2)
                .get();


        System.out.println(oldestUser.getPersonDetails().toString());

        return oldestUser.getPersonDetails();
    }

   public static User findUserWithLongestUsername(List<User> users) {


       Comparator<User> comp = (u1,u2)-> Integer.compare(u1.getName().length(),u2.getName().length());

       User longest = users.stream().max(comp).get();

       System.out.println(longest.toString());

        return longest;
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {

       String above18 = users.stream()
                .filter(p->p.getPersonDetails().getAge()>=18)
                .map(u->u.getPersonDetails().getName() + " " + u.getPersonDetails().getSurname())
                .collect(Collectors.joining(" and ", "", " are of legal age here."));

       System.out.println(above18);

        return above18;

    }


    public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {


        List<String> sortedList =  users.stream()
                .filter(u->u.getName().startsWith("A"))
                .flatMap(u->u.getPersonDetails().getRole().getPermissions().stream())
                .map(p->p.getName())
                .sorted((p1,p2)->p1.compareTo(p2))
                .collect(Collectors.toList());

        for(String item:sortedList){
            System.out.print(item);
            System.out.println();
        }

        return sortedList;

    }

    public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {

            users.stream()
                .filter(p->p.getPersonDetails().getSurname().startsWith("S"))
                .flatMap(u->u.getPersonDetails().getRole().getPermissions().stream())
                .forEach(u->System.out.println(u.getName()));

    }

    public static Map<Role, List<User>> groupUsersByRole(List<User> users) {

        Map <Role,List<User>> roleGrouped = users.stream()
                .collect(Collectors.groupingBy(u->u.getPersonDetails().getRole()));

         for(Map.Entry entry : roleGrouped.entrySet()){
             System.out.println(entry.getKey() + ", " + entry.getValue());
         }

         return roleGrouped;
    }

    public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {

        Map <Boolean,List<User>> partition = users.stream()
        .collect(Collectors.partitioningBy(user -> user.getPersonDetails().getAge() > 18));

        return partition;

    }


}
