
import Domain.*;
import Service.UserService;

import java.util.Arrays;
import java.util.List;

public class Main {

    private static List<String> phonenumbers;
    private static List<User> users;
    private static List<Person> people;
    private static List<Address> addresses;
    private static List<Permission> permissions;
    private static List<Role> roles;

    private static List<String> initPhoneNumberList(){

        String number1 = "1239910219";
        String number2 = "291012919";
        String number3 = "12910102122";

        phonenumbers = Arrays.asList(number1,number2,number3);

        return phonenumbers;
    }

    private static void initRoleList(){

        Role Admin = new Role("Admin",initAdminPermission());
        Role User = new Role("User", initUserPermission());
        Role Banned = new Role("Banned",initBannedPermission());

        roles = Arrays.asList(Admin,User,Banned);

    }

    private static List<Permission> initAdminPermission(){

        Permission perm1 = new Permission("Add Posts");
        Permission perm2 = new Permission("Watch posts");
        Permission perm3 = new Permission("Delete posts");
        Permission perm4 = new Permission("Ban");
        Permission perm5 = new Permission("Remake posts");

        permissions = Arrays.asList(perm1,perm2,perm3,perm4,perm5);

        return permissions;

    }

    private static List<Permission> initUserPermission(){
        Permission perm1 = new Permission("Add posts");
        Permission perm2 = new Permission("Watch posts");

        permissions = Arrays.asList(perm1,perm2);

        return permissions;
    }

    private static List<Permission> initBannedPermission(){
        Permission perm1 = new Permission("Watch posts");

        permissions = Arrays.asList(perm1);

        return permissions;
    }

    private static List<Person> initPersonList(){
        initRoleList();
        initAddressList1();
        initAddressList2();
        initPhoneNumberList();

        Person person1 = new Person("Jan","Kowalski",initPhoneNumberList(),initAddressList1(),
                roles.get(1),17);

        Person person2 = new Person("Kazimierz","Sakistam",initPhoneNumberList(),initAddressList2(),
                roles.get(0),30);

        Person person3 = new Person("JakisTyp","Sakies",initPhoneNumberList(),initAddressList2(),
                roles.get(2),22);

        people = Arrays.asList(person1,person2,person3);

        return people;

    }

    private static List<Address> initAddressList1(){

        Address address1 = new Address("Cieszynskiego",16,4,"Gdansk",
                "32-200","Poland");
        Address address2 = new Address("kosciuszki",6,1,"Lidzbark Warminski",
                "11-100","Polska");

        addresses = Arrays.asList(address1,address2);

        return addresses;

    }

    private static List<Address> initAddressList2(){

        Address address1 = new Address("Cieszynskiego",16,4,"Gdansk",
                "32-200","Poland");

        addresses = Arrays.asList(address1);

        return addresses;
    }

    private static void initUsersList(){
        initPersonList();

        User user1 = new User("AJankos","1234567",people.get(0));
        User user2 = new User("Aaaa","damskld",people.get(1));
        User user3 = new User("daskldmlaa","dmaskda",people.get(2));

        users = Arrays.asList(user1,user2,user3);

    }

    public static void main(String[] args) {

        initUsersList();

        //UserService.findUsersWhoHaveMoreThanOneAddress(users);
        //UserService.findOldestPerson(users);
        //UserService.findUserWithLongestUsername(users);
        UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users);
        // UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users);
        // UserService.printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(users);
        // UserService.groupUsersByRole(users);

    }
}
